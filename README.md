# Wordfilter

The Wordfilter module provides a suitable base for manual filtering of profanity
("bad words") as well as for dynamic keyword replacement.

The user can choose between different filtering processes, like direct filtering
of specified words or token filtering. The user may add their own
implementation, e.g. for using an external web service which does the filtering
or you.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/wordfilter).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/wordfilter).


## Table of contents

- Requirements
- Installation
- Configuration
- Use in custom module
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Navigate to Administration > Extend and enable the module.
1. Navigate to Administration > Configuration > Content Authoring >
   Wordfilter configurations to create and manage Wordfilter configurations.
1. Add a new configuration. Select the Implementation to be used to filter
   the specific words: Direct substitution or Token substitution.
1. Enter in the Substitution text, any filtered word will be replaced by
   this substitution text (optional).
1. Enter the words to filter in a comma-separated list.
1. Save.
1. Navigate to Administration > Configuration > Content Authoring > Text
   formats and editors to configure the text editor.
1. Select the text format to edit and check the "Apply filtering of
   words" to filter out words. Once checked, a filter settings tab will
   open and the user can select the desired Active Wordfilter
   configurations.
1. Save configuration.


## Use in custom module

It's easy to implement Wordfilter in your custom module, simply by using it as a filter. Follow the steps outlined above, and then process the variable in question with the selected format (in this example `plain_text` is used) with this code:

```
$raw_string = "A string to remove some bad words from";
$cleaned_text = [
  '#type' => 'processed_text',
  '#text' => $raw_string,
  '#format' => 'plain_text',
];
```


## Maintainers

- Maximilian Haupt - [mxh](https://www.drupal.org/u/mxh)

Supporting organization:

- [Hubert Burda Media](https://www.drupal.org/hubert-burda-media)
